package com.hcl.learn.exception;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ErrorResponse {
//    private HttpStatus code;
	private int code;
    private String message;
}
