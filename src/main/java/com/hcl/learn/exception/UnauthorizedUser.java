package com.hcl.learn.exception;

public class UnauthorizedUser extends EcommerceApplicationException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	 public UnauthorizedUser(){
        super("Unauthourizd User", GlobalErrorCode.ERROR_UNAUTHOURIZED_USER);
    }

    public UnauthorizedUser(String message) {
        super(message, GlobalErrorCode.ERROR_UNAUTHOURIZED_USER);
    }
}

