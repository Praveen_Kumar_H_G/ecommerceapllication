package com.hcl.learn.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.learn.entity.UserRegistration;

public interface UserRepository extends JpaRepository<UserRegistration, Integer> {

}
